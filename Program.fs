﻿printfn "Battleship"

open Board


let loadBoardSetupExamples () =

    printf "\n# Board Setup Examples #\n\n"

    [ BoardExamples.noColumnsOrRows
    ; BoardExamples.noColumns
    ; BoardExamples.noRows
    ; BoardExamples.noShips
    ; BoardExamples.invalidShipLength
    ; BoardExamples.outOfBoundsShip1
    ; BoardExamples.outOfBoundsShip2
    ; BoardExamples.outOfBoundsShip3
    ; BoardExamples.outOfBoundsShip4
    ; BoardExamples.intersectingShips1
    ; BoardExamples.intersectingShips2
    ; BoardExamples.intersectingShips3
    ; BoardExamples.happyBoard1
    ]
    |> List.iteri
        (fun index (data, expectation) ->
            let (size, ships) = data
            let result = (create size ships)
            let status =
                match (expectation, result) with
                | (Some err1, Error err2) when err1 = err2 -> "Pass"
                | (None, Ok _) -> "Pass" // treat as opaque
                | (_, _) -> "Fail"

            printfn "\n"
            printfn "# Test %i : %s #\n" (index + 1) status
            printfn "%A\n" data
            if status = "Fail"
                then
                    printfn "## Actual ##\n%A\n" result
                    printfn "## Expected Error ##\n%A\n" expectation
                else ()
        )


let logThen value =
    printfn "%A" value
    value

let logAndThen message value =
    printfn "\n%s" message
    printfn "%A" value
    value

let scenario1 () =
    printf "\n# Scenario 1 #\n\n"

    ((10<column>, 10<row>), [(5<column>, 5<row>, Vertical 1<row>)])
    ||> create
    |> logAndThen "Created board:"
    |> Result.bind (targetAndFire (5<column>, 5<row>))
    |> logAndThen $"Targeted and Fired: {(5<column>, 5<row>)}"
    |> ignore

let scenario2 () =
    printf "\n# Scenario 2 #\n\n"

    ((10<column>, 10<row>), [(8<column>, 1<row>, Vertical 2<row>)])
    ||> create
    |> logAndThen "Created board:"
    |> Result.bind (target (8<column>, 1<row>))
    |> logAndThen $"Targeted: {(8<column>, 1<row>)}"
    |> Result.map fire
    |> logAndThen "Fired:"
    |> Result.map snd
    |> Result.bind (targetAndFire (3<column>, 3<row>))
    |> logAndThen $"Targeted and Fired: {(3<column>, 3<row>)}"
    |> Result.bind (targetAndFire (8<column>, 2<row>))
    |> logAndThen $"Targeted and Fired: {(8<column>, 2<row>)}"
    |> ignore

let scenario3 () =
    printf "\n# Scenario 3 #\n\n"

    (fst BoardExamples.happyBoard1)
    ||> create 
    |> logAndThen "Created board:"
    |> Result.bind (targetAndFire (1<column>, 1<row>))
    |> logAndThen $"Targeted and Fired: {(1<column>, 1<row>)}"
    |> Result.bind (targetAndFire (8<column>, 1<row>))
    |> logAndThen $"Targeted and Fired: {(8<column>, 1<row>)}"
    |> Result.bind (targetAndFire (9<column>, 1<row>))
    |> logAndThen $"Targeted and Fired: {(9<column>, 1<row>)}"
    |> Result.bind (targetAndFire (8<column>, 2<row>))
    |> logAndThen $"Targeted and Fired: {(8<column>, 2<row>)}"
    |> Result.bind (targetAndFire (3<column>, 2<row>))
    |> logAndThen $"Targeted and Fired: {(3<column>, 2<row>)}"
    |> Result.bind (targetAndFire (4<column>, 2<row>))
    |> logAndThen $"Targeted and Fired: {(4<column>, 2<row>)}"
    |> Result.bind (targetAndFire (5<column>, 2<row>))
    |> logAndThen $"Targeted and Fired: {(5<column>, 2<row>)}"
    |> ignore

    
loadBoardSetupExamples ()
scenario1 ()
scenario2 ()
scenario3 ()