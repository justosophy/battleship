# Battleship Module

> Justin Anderson


## Dependencies

Ensure you have at least dotnet version 6.

In a terminal run

> `dotnet --version`
>
> 6.0.201

## Run

In a terminal go to the repository root-directory and run

> `dotnet run`

## Reading through

Begin with `src/Board.fsi`. This shows the signature and exposed bindings for the `Board` module. As shown in the signature, this module only contains types and functions relevant implement a Battleship state-tracker for a single player. This and `src/Board.fs` is the core of task.

In `src/BoardExamples.fs` is some test data for different board initialization values.

In `Program.fs` are a few scenarios to test board creation and view state transitions. This is what runs should you build and run.

The `Board` module represents the creation and transition of the Battleship board states.

The central `Board` type is opaque to external access by the module. Access to the `BoardData` is available though the `get` function, but this cannot be used again as input to create or modify the `Board`. Validation of board dimensions, ships, and targeting is parsed through the module functions. The opaque type and validation parsing is to ensure an illegal state for the `Board` is unrepresentable by an external user of the module.

The motivation is that
- When creating a board, one is sure to get back a valid board or a descriptive error.
- When targeting, one is sure to have a valid board to target on that isn't already targeted
- When firing, one is sure to have a valid target locked in, the success response, and a reconciled new state for the board to use as the result.

## Considerations

### Readability

I aimed to be idiomatic according to the language (F#).

Separate the internals from the external with the signature. Keep the exposed bindings minimal.

Group like-behavior into public and private submodules. This lends itself easily to refactoring should things expand later.

Make lite but sensible use of Units of Measure (`column` and `row`) and constructors (`Vertical` and `Horizontal`, `Hit` and `Missed`) for localized and self-documenting expressions instead of more generic types.

Phantom Types (`Default` and `Targeted`) provide module users with compilation error and type hints so as to not try to `fire` on a board that hasn't yet produced a valid target.

### Maintainability

The module abstraction layer for `Board` stays thin to the raw input and data type - while keeping the firm validation-parsing distinction. Too much indirection or too thick a layer inbetween risks maintainability. By ensuring the shared primitive types are passed through I reduce the likelihood of expanding the abstraction layers for extension and debugging.


### Testability

The strict interface for the board state-tracking lends itself to testability. All transitions are parsed at runtime or checked at compile time.

Each transition is an expression, a pure function, and parsing returns consistent errors.

The Program runtime demonstrates how tests and scenarios are predictable before even adding external test frameworks.

### Extensibility

The type lend to refactoring with more confidence with compiler/LSP hints.

The functions are pure and compose over the central `Board` type and over the standard F# types (e.g. `Result<Board, List<string>)`).

The `BoardData` record and `Board` constructor lend to ordinary product-expansion for new information as needed.

The constrained type under `BoardStatus` lends to expanding the module functions while adding qualified restrictions.

While the module's functions are largely independent of each other, the ADT approach obviously lives on the behavioral axis of Expression Problem ;)
