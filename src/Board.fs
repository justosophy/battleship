module Board

[<Measure>] type column
[<Measure>] type row
type Length = | Vertical of int<row> | Horizontal of int<column>
type ShipCoordinates = int<column> * int<row> * Length
type Damage = | Hit | Miss
type Fires = Map<int<column> * int<row>, Damage>

type BoardData = {
    Columns: int<column>;
    Rows: int<row>;
    Ships: List<ShipCoordinates>;
    Target: Option<int<column> * int<row>>;
    Fires: Fires;
}

module CreationErrors =
    let lowColumnsErrorMessage = "Columns must be 3 or more"
    let lowRowsErrorMessage = "Rows must be 3 or more"
    let noShips = "A minimum of one ship is required"
    let invalidShipLength = "Ship cannot have a zero or negative length"
    let outOfBoundsShip c r l = $"Ship (col: {c}, row: {r}, len: {l}) is out of bounds"
    let intersectShip ship1 ship2 =
        let (c, r, l) = ship1
        let (c2, r2, l2) = ship2
        $"Ship (col: {c}, row: {r}, len: {l}) intersects Ship (col: {c2}, row: {r2}, len: {l2})"

module BoardVerification =
    let (|InvalidShipLength|_|)
        ( columns: int<column>
        , rows: int<row>
        , previousShips: List<ShipCoordinates>
        , ship: ShipCoordinates)
        : Option<List<string>> =
            match ship with
            | (_, _, Vertical l) when l < 1<row> ->
                Some [ CreationErrors.invalidShipLength ]
            | (_, _, Horizontal l) when l < 1<column> ->
                Some [ CreationErrors.invalidShipLength ]
            | _ -> None

    let (|OutOfBoundsShip|_|)
        ( columns: int<column>
        , rows: int<row>
        , previousShips: List<ShipCoordinates>
        , ship: ShipCoordinates)
        : Option<List<string>> =
            match ship with
            | (column, row, length)
                when column < 1<column>
                || column > columns
                || row < 1<row>
                || row > rows
                ->
                Some [ CreationErrors.outOfBoundsShip column row length ]
            | (column, row, Vertical l)
                when row + (l - 1<row>) > rows ->
                    Some [ CreationErrors.outOfBoundsShip column row (Vertical l) ]
            | (column, row, Horizontal l)
                when column + (l - 1<column>) > columns ->
                    Some [ CreationErrors.outOfBoundsShip column row (Horizontal l) ]
            | _ -> None

    let shipsIntersect ((col1, row1, len1): ShipCoordinates) ((col2, row2, len2): ShipCoordinates) =
        match (len1, len2) with
        | (Horizontal l1, Horizontal l2) when row1 = row2 ->
            max col1 col2 <= min (col1 + l1 - 1<column>) (col2 + l2 - 1<column>)
        | (Vertical l1, Vertical l2) when col1 = col2 ->
            max row1 row2 <= min (row1 + l1 - 1<row>) (row2 + l2 - 1<row>)
        | (Horizontal l1, Vertical l2) ->
            col1 <= col2 && (col1 + l1 - 1<column>) >= col2
            && row2 <= row1 && (row2 + l2 - 1<row>) >= row1
        | (Vertical l1, Horizontal l2) ->
            col2 <= col1 && (col2 + l2 - 1<column>) >= col1
            && row1 <= row2 && (row1 + l1 - 1<row>) >= row2
        | _ -> false

    let (|OverlappingShip|_|)
        ( columns: int<column>
        , rows: int<row>
        , previousShips: List<ShipCoordinates>
        , ship: ShipCoordinates)
        : Option<List<string>> =
        let intersection  = previousShips |> List.tryFind (shipsIntersect ship)
        match intersection with
        | Some ship2 ->
            Some [ CreationErrors.intersectShip ship ship2 ]
        | None -> None

    let verifyShipPlacement
        (columns: int<column>)
        (rows: int<row>)
        (previousShips: List<ShipCoordinates>)
        (ship: ShipCoordinates) =
        match (columns, rows, previousShips, ship) with
        | InvalidShipLength errors -> Error errors
        | OutOfBoundsShip errors -> Error errors
        | OverlappingShip errors -> Error errors
        | _ -> Ok <| List.append previousShips [ ship ]

    let verifyShips (columns: int<column>, rows: int<row>, ships: List<ShipCoordinates>) =
        (Ok [] , ships)
        ||> List.fold
            (fun acc ship ->
                acc |> Result.bind
                    (fun shipsAcc ->
                        verifyShipPlacement columns rows shipsAcc ship
                    )
            )
            

    let (|InvalidBoardDimensions|_|) (columns: int<column>, rows: int<row>, _) =
        match (columns, rows) with
        | (c, r) when c < 3<column> && r < 3<row> ->
            Some [CreationErrors.lowColumnsErrorMessage ; CreationErrors.lowRowsErrorMessage]
        | (c, _) when c < 3<column> ->
            Some [CreationErrors.lowColumnsErrorMessage ]
        | (_, r) when r < 3<row> ->
            Some [CreationErrors.lowRowsErrorMessage ]
        | _ -> None


    let (|InvalidShips|ValidPlacement|)
        (columns: int<column>, rows: int<row>, ships: List<ShipCoordinates>)
        : Choice<list<string>,unit> =
        match (columns, rows, ships) with
        | (_, _, []) -> InvalidShips [ CreationErrors.noShips ]
        | _ ->
            match verifyShips (columns, rows, ships) with
            | Error errors -> InvalidShips errors
            | Ok _ -> ValidPlacement

type BoardStatus = interface end
[<Interface>] type Default = inherit BoardStatus
[<Interface>] type Targeted = inherit BoardStatus

type Board<'t> when 't :> BoardStatus = private Board of BoardData

let create
    (columns: int<column>, rows: int<row>)
    (ships: List<ShipCoordinates>):
    Result<Board<Default>,list<string>> =
    match (columns, rows, ships) with
    | BoardVerification.InvalidBoardDimensions errors -> Error errors
    | BoardVerification.InvalidShips errors -> Error errors
    | BoardVerification.ValidPlacement ->
        Ok <|
            Board {
                Columns = columns;
                Rows = rows;
                Ships = ships;
                Target = None;
                Fires = Map.empty;
            }

let get (Board data) = data

let target
    (column: int<column>, row: int<row>)
    (board: Board<Default>):
    Result<Board<Targeted>, List<string>> =
        let data = get board
        if column >= 1<column>
            && column <= data.Columns
            && row >= 1<row>
            && row <= data.Rows
        then 
            Ok <| Board { data with Target = Some (column, row) }
        else
            Error [ "Target coordinates are outside of the board" ]

let clearTarget
    (board: Board<Targeted>):
    Board<Default> =
        let data = get board
        Board { data with Target = None }

let checkHit
    (column: int<column>, row: int<row>)
    (ships: List<ShipCoordinates>)
    : Option<int> =
        ships
        |> List.tryFindIndex
            (function
                | (shipCol, shipRow, Horizontal length) ->
                    row = shipRow
                    && column >= shipCol
                    && column <= shipCol + length - 1<column>
                | (shipCol, shipRow, Vertical length) ->
                    column = shipCol
                    && row >= shipRow
                    && row <= shipRow + length - 1<row>
            )

let isShipDamageComplete
    (ship: ShipCoordinates)
    (fires: Fires):
    bool =
        match ship with
        | (column, row, Horizontal length) ->
            let shipCol = int (unbox column)
            let shipLen = int (unbox length)
            [ for i in [shipCol..(shipCol + shipLen - 1)] -> i * 1<column> ]
            |> List.forall
                (fun cellCol ->
                    match Map.tryFind (cellCol, row) fires with
                    | Some Hit -> true
                    | _ -> false
                )

        | (column, row, Vertical length) ->
            let shipRow = int (unbox row)
            let shipLen = int (unbox length)
            [ for i in [shipRow..(shipRow + shipLen - 1)] -> i * 1<row> ]
            |> List.forall
                (fun cellRow ->
                    match Map.tryFind (column, cellRow) fires with
                    | Some Hit -> true
                    | _ -> false
                )

let fire
    (board: Board<Targeted>):
    Damage * Board<Default> =
        let data = get board
        // Assume `Option.get` is safe due to `Board<Targeted>`
        let target = (Option.get data.Target)
        let hit = checkHit target data.Ships

        match hit with
        | Some shipIndex ->
            // Assume index get is safe due to `checkHit`
            let fires = data.Fires |> Map.add target Hit
            let ship = data.Ships.[shipIndex]

            let ships =
                if isShipDamageComplete ship fires
                    then data.Ships |> List.removeAt shipIndex
                    else data.Ships

            ( Hit
            , Board { data with Target = None; Fires = fires; Ships = ships }
            )

        | None ->
            let fires = data.Fires |> Map.add target Miss
            ( Miss
            , Board { data with Target = None; Fires = fires }
            )
        
let targetAndFire
    (column: int<column>, row: int<row>)
    (board: Board<Default>):
    Result<Board<Default>,List<string>> =
    target (column, row) board
     |> Result.map fire
     |> Result.map snd