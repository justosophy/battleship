module Board

[<Measure>]
type column

[<Measure>]
type row

type Length =
    | Vertical of int<row>
    | Horizontal of int<column>

type ShipCoordinates = int<column> * int<row> * Length

type Damage = | Hit | Miss
type Fires = Map<int<column> * int<row>, Damage>

type BoardData = {
    Columns: int<column>;
    Rows: int<row>;
    Ships: List<ShipCoordinates>;
    Target: Option<int<column> * int<row>>;
    Fires: Fires;
}

module CreationErrors =
    val lowColumnsErrorMessage: string
    val lowRowsErrorMessage: string
    val noShips: string
    val invalidShipLength: string
    val outOfBoundsShip: c: 'a -> r: 'b -> l: 'c -> string
    val intersectShip: 'a * 'b * 'c -> 'd * 'e * 'f -> string

type BoardStatus = interface end
[<Interface>] type Default = inherit BoardStatus
[<Interface>] type Targeted = inherit BoardStatus
type Board<'t> when 't :> BoardStatus = private Board of BoardData

val create:
    columns: int<column> * rows: int<row> -> ships: List<ShipCoordinates>
        -> Result<Board<Default>,List<string>>

val get:
    Board<'a> -> BoardData

val target:
   column: int<column> * row : int<row> ->
   board : Board<Default>
        -> Result<Board<Targeted>,List<string>>
    
val clearTarget:
   board : Board<Targeted>
        -> Board<Default>

val fire:
   board: Board<Targeted>
       -> Damage * Board<Default>

val targetAndFire:
   column: int<column> * row : int<row> ->
   board : Board<Default>
        -> Result<Board<Default>,List<string>>