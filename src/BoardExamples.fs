module BoardExamples

open Board
module E = Board.CreationErrors


let noColumnsOrRows =
    (*
        ~
    *)
    ( ( (0<column>, 0<row>), [ (3<column>, 3<row>, Horizontal 2<column>) ] )
    , Some [ E.lowColumnsErrorMessage; E.lowRowsErrorMessage ]
    )

let noColumns =
    (*
        ~
        ~
        ~
        ~
        ~
        ~
        ~
        ~
        ~
        ~
    *)
    ( ( (0<column>, 10<row>), [ (3<column>, 3<row>, Horizontal 2<column>) ] )
    , Some [ E.lowColumnsErrorMessage ]
    )

let noRows =
    (*
        ~~~~~~~~~~
    *)
    ( ( (10<column>, 0<row>), [ (3<column>, 3<row>, Horizontal 2<column>) ] )
    , Some [ E.lowRowsErrorMessage ]
    )

let noShips =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    ( ((10<column>, 10<row>), [ ] )
    , Some [ E.noShips ]
    )

let invalidShipLength =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
        XX?XXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    ( ((10<column>, 10<row>), [ (3<column>, 3<row>, Horizontal -2<column>) ] )
    , Some [ E.invalidShipLength ]
    )

let outOfBoundsShip1 =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
      ███XXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    ( ((10<column>, 10<row>), [ (-2<column>, 3<row>, Horizontal 3<column>) ] )
    , Some [ E.outOfBoundsShip -2<column> 3<row> (Horizontal 3<column>) ]
    )

let outOfBoundsShip2 =
    (*
          █
          █
        XX█XXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    ( ((10<column>, 10<row>), [ (3<column>, -2<row>, Vertical 3<row>) ] )
    , Some [ E.outOfBoundsShip 3<column> -2<row> (Vertical 3<row>) ]
    )

let outOfBoundsShip3 =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXX████
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    ( ((10<column>, 10<row>), [ (8<column>, 3<row>, Horizontal 4<column>) ] )
    , Some [ E.outOfBoundsShip 8<column> 3<row> (Horizontal 4<column>) ]
    )

let outOfBoundsShip4 =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XX█XXXXXXX
        XX█XXXXXXX
        XX█XXXXXXX
          █
    *)
    ( ((10<column>, 10<row>), [ (3<column>, 8<row>, Vertical 4<row>) ] )
    , Some [ E.outOfBoundsShip 3<column> 8<row> (Vertical 4<row>) ]
    )

let intersectingShips1 =
    (*
        XXXXXXXXXX
        XXXXXXXXXX
        XXX!!!XXXX <- duplicate
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    let ship = (3<column>, 3<row>, Horizontal 2<column>)
    ( ((10<column>, 10<row>), [ ship; ship ] )
    , Some [ E.intersectShip ship ship ]
    )

let intersectingShips2 =
    (*
        XXXX█XXXXX
        XXXX█XXXXX
        X███!███XX
        XXXX█XXXXX
        XXXX█XXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXXXXXXXX
    *)
    let ship1 = (5<column>, 1<row>, Vertical 5<row>)
    let ship2 = (2<column>, 3<row>, Horizontal 7<column>)
    ( ((10<column>, 10<row>), [ ship1; ship2 ] )
    , Some [ E.intersectShip ship2 ship1 ]
    )

let intersectingShips3 =
    (*
        XXXX█XXXXX
        XXXX█XXXXX
        X██X█XXXXX
        XXXX█XXXXX
        XXXX█XXXXX
        XXXXXXXXXX
        XXXXXXXXXX
        XXXX█XXXXX
        X███!XXXXX
        XXXX█XXXXX
    *)
    let ship1 = (5<column>, 1<row>, Vertical 5<row>)
    let ship2 = (2<column>, 3<row>, Horizontal 2<column>)
    let ship3 = (2<column>, 9<row>, Horizontal 4<column>)
    let ship4 = (5<column>, 8<row>, Vertical 3<row>)
    ( ((10<column>, 10<row>), [ ship1; ship2; ship3; ship4 ] )
    , Some [ E.intersectShip ship4 ship3 ]
    )

let happyBoard1 =
    (*
        XXXXXXX█XX <- Ship1
Ship2-> XX███XX█XX
        XXXXXXXXXX
        XXXX████XX <- Ship3
      ┍ X█XXXXXXXX
Ship4─┤ X█XXX█████ <- Ship5
      │ X█XXXXXXXX
      ┕ X███████XX <─┐
        XXXXXXXXXX   │
        XXXXXXXXXX   │
          ┖────┸─Ship6
    *)
    let ship1 = (8<column>, 1<row>, Vertical 2<row>)
    let ship2 = (3<column>, 2<row>, Horizontal 3<column>)
    let ship3 = (5<column>, 4<row>, Horizontal 4<column>)
    let ship4 = (2<column>, 5<row>, Vertical 4<row>)
    let ship5 = (6<column>, 6<row>, Horizontal 5<column>)
    let ship6 = (3<column>, 8<row>, Horizontal 6<column>)
    let ships = [ ship1; ship2; ship3; ship4; ship5; ship6 ]
    ( ((10<column>, 10<row>), ships )
    , None
    )
